<?php
	include ('../logica/session.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style>
.aviso3 
{
	font-size: 130%;
	font-weight: bold;
	color: #11a9e3;
	text-transform:uppercase;
	/*font-family: "Trebuchet MS";
	font-family:"Gill Sans MT";
	border-radius:10px;
	background: #11a9e3;*/
	background-color:transparent;
	text-align: center;
	padding:10px;
}
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
</head>
<body>
<?php
$string_intro = getenv("QUERY_STRING"); 
parse_str($string_intro);
require('../datos/conex.php');
if(isset($_POST['gestion']))
{
	$gestion=$_POST['gestion'];
}
else
{
	$gestion='';
}
$codigo_gestion=$_POST['codigo_gestion'];
$codigo_usuario2=$_POST['codigo_usuario2'];
$codigo_usuario=$_POST['codigo_usuario'];
$estado_paciente=$_POST['estado_paciente'];
$status_paciente='N/A';
$fecha_activacion=$_POST['fecha_activacion'];
$nombre=$_POST['nombre'];
$apellidos=$_POST['apellidos'];
$identificacion=$_POST['identificacion'];
$telefono1=$_POST['telefono1'];
$telefono2=$_POST['telefono2'];
$telefono3=$_POST['telefono3'];
$correo=$_POST['correo'];
$ciudad=$_POST['ciudad'];
$fecha_retiro=$_POST['fecha_retiro'];
$motivo_retiro=$_POST['motivo_retiro'];
$observacion_retiro=$_POST['observacion_retiro'];
$cambio_estado_paciente = $_POST['cambio_estado_paciente'];
if($cambio_estado_paciente != 'No')
	{
         include("../presentacion/email/mail_estado.php");
$INSERT_CAMBIO_ESTADO=mysqli_query($conex, "INSERT INTO bayer_cambio_estado (FECHA_SOLICITUD, PAP, NOMBRE, ESTADO_ACTUAL, NUEVO_ESTADO, ASESOR) VALUES(CURRENT_TIMESTAMP, '".$codigo_usuario."', '".$nombre.' '.$apellidos."', '".$estado_paciente."', '".$cambio_estado_paciente."', '".$usuname."')");
echo mysqli_error($conex);
	}
$direccion_nueva=$_POST['DIRECCION'];
if($direccion_nueva!='')
{
	$direccion=$direccion_nueva;
}
if($direccion_nueva=='')
{
	$direccion=$_POST['direccion_act'];
}
$barrio=$_POST['barrio'];
$departamento=$_POST['departamento'];
$fecha_nacimiento=$_POST['fecha_nacimiento'];
$fecha_ini_terapia=$_POST['fecha_ini_terapia'];
$edad=$_POST['edad'];
$operador_logistico='N/A';
$punto_entrega=$_POST['punto_entrega'];
$motivo_comunicacion=$_POST['motivo_comunicacion'];
$medio_contacto=$_POST['medio_contacto'];
$tipo_llamada=$_POST['tipo_llamada'];
if(isset($_POST['logro_comunicacion']))
{
	$logro_comunicacion=$_POST['logro_comunicacion'];
}
else
{
	$logro_comunicacion='';
}
$motivo_no_comunicacion=$_POST['motivo_no_comunicacion'];
$via_recepcion=$_POST['via_recepcion'];
$asegurador=$_POST['asegurador'];
$ips_atiende='N/A';
if($_POST['medico']=='Otro')
{
	$medico=$_POST['medico_nuevo'];
	$INSERT_MEDICO=mysqli_query($conex, "INSERT INTO bayer_listas(MEDICO)VALUES('".$medico."')");
	echo mysqli_error($conex);	
}
else
{
	$medico=$_POST['medico'];	
}
$estado_ctc='N/A';
if(isset($_POST['dificultad_acceso']))
{
	$dificultad_acceso=$_POST['dificultad_acceso'];
}
else
{
	$dificultad_acceso='';
}
$tipo_dificultad=$_POST['tipo_dificultad'];
if(isset($_POST['envios']))
{
	$envios=$_POST['envios'];
}
else
{
	$envios='';
}
if($_POST['tratamiento_previo'] == 'Otro')
{
	$tratamiento_previo=$_POST['tratamiento_previo_otro'];
}
else
{	
    $tratamiento_previo=$_POST['tratamiento_previo'];
}
$MEDICAMENTO=$_POST['MEDICAMENTO'];
if($MEDICAMENTO=='Xofigo 1x6 ml CO')
{
	$dosis=$_POST['Dosis2'];
}
if($MEDICAMENTO=='KOGENATE FS 2000 PLAN')
{
	$dosis=$_POST['Dosis3'];
}
if($MEDICAMENTO!='Xofigo 1x6 ml CO'&&$MEDICAMENTO!='KOGENATE FS 2000 PLAN')
{
	$dosis=$_POST['Dosis'];
}
$dosis;
//$tipo_envio=$_POST['tipo_envio'];
$tipo_envio='';
if(isset($_POST['evento_adverso']))
{
	$evento_adverso=$_POST['evento_adverso'];
}
else
{
	$evento_adverso='';
}
$tipo_evento_adverso=$_POST['tipo_evento_adverso'];
if(isset($_POST['genera_solicitud']))
{
	$genera_solicitud=$_POST['genera_solicitud'];
}
else
{
	$genera_solicitud='';
}
$fecha_proxima_llamada=$_POST['fecha_proxima_llamada'];
$motivo_proxima_llamada=$_POST['motivo_proxima_llamada'];
$observacion_proxima_llamada=$_POST['observacion_proxima_llamada'];
$reclamo=$_POST['reclamo'];
if(isset($_POST['registrar']))
{
	if($reclamo=='SI')
	{
		$fecha_actual=date('Y-m-d');
		$fecha_reclamacion=$_POST['fecha_reclamacion'];
		$fecha_rec = explode("-", $fecha_reclamacion);
		$anio=$fecha_rec[0]; // año
		$mes=$fecha_rec[1]; // mes
		$dia=$fecha_rec[2]; // dia
		$fecha_actual=date('Y-m-d');
		$fecha_rec_act = explode("-", $fecha_actual);
		$mes_act=$fecha_rec_act[1]; // mes
		$dato=((int)$mes);
		$numero_cajas=$_POST['numero_cajas'].' '.$_POST['tipo_numero_cajas'];
	}
	if($fecha_reclamacion=='')
	{
		$fecha_reclamacion=$_POST['fecha_ultima_reclamacion'];
		$fecha_ultima_reclamacion=$_POST['fecha_ultima_reclamacion'];
	}
	if($reclamo=='NO')
	{
		$fecha_reclamacion='';
		$fecha_actual=date('Y-m-d');
		$fecha_rec_act = explode("-", $fecha_actual);
		$anio_act=$fecha_rec_act[0]; // año
		$mes_act=$fecha_rec_act[1]; // mes
		$dia_act=$fecha_rec_act[2]; // dia
		$dato=((int)$mes_act);
		$fecha_ultima_reclamacion=$_POST['fecha_ultima_reclamacion'];
		if(isset($_POST['causa_no_reclamacion']))
		{
			$causa_no_reclamacion=$_POST['causa_no_reclamacion'];
		}
		else
		{
			$causa_no_reclamacion='';
		}
		$numero_cajas='0 Aplicacion';
	}
	if($genera_solicitud == 'SI')
	{
		 include("../presentacion/email/mail_novedades.php");
	}
	$consecutivo=$_POST['consecutivo'];
$numero_nebulizaciones=$_POST['nebulizaciones'];
$estado_farmacia='N/A';
$consecutivo_betaferon=$_POST['consecutivo_betaferon'];
/*OPERADOR_LOGISTICO_TRATAMIENTO='".$operador_logistico."'*/
//$autor=$_POST['autor'];
$autor=$usuname_peru;
$numero_tabletas_diarias=$_POST['numero_tabletas_diarias'];
$descripcion_comunicacion=$_POST['descripcion_comunicacion'];
$nota=$_POST['nota'];
if(isset($_POST['registrar']))
{
	$select_historial=mysqli_query($conex, "SELECT * FROM bayer_historial_reclamacion WHERE ID_PACIENTE_FK='$codigo_usuario2'");
	echo mysqli_error($conex);
	$reg_hist=mysqli_num_rows($select_historial);
	if($reg_hist>0)
	{
		if($reclamo=='SI')
		{
			$UPDATE_HISTORIAL=mysqli_query($conex, "UPDATE bayer_historial_reclamacion SET  RECLAMO$dato='".$reclamo."',FECHA_RECLAMACION$dato='".$fecha_reclamacion."',MOTIVO_NO_RECLAMACION$dato='' WHERE ID_PACIENTE_FK='".$codigo_usuario2."' AND MES$dato='".$mes."'");
			echo mysqli_error($conex);
		}
		if($reclamo=='NO')
		{
			$UPDATE_HISTORIAL=mysqli_query($conex, "UPDATE bayer_historial_reclamacion SET  RECLAMO$dato='".$reclamo."',MOTIVO_NO_RECLAMACION$dato='".$causa_no_reclamacion."',FECHA_RECLAMACION$dato='' WHERE ID_PACIENTE_FK='".$codigo_usuario2."' AND MES$dato='".$mes_act."'");
			echo mysqli_error($conex);
		}
	}
	else
	{
		$INSERT_HISTORIAL=mysqli_query($conex, "INSERT INTO bayer_historial_reclamacion(ID_PACIENTE_FK) VALUES('".$codigo_usuario2."')");
		echo mysqli_error($conex);
		if($reclamo=='SI')
		{
			$UPDATE_HISTORIAL=mysqli_query($conex, "UPDATE bayer_historial_reclamacion SET  RECLAMO$dato='".$reclamo."',FECHA_RECLAMACION$dato='".$fecha_reclamacion."',MOTIVO_NO_RECLAMACION$dato='' WHERE ID_PACIENTE_FK='".$codigo_usuario2."' AND MES$dato='".$mes_act."'");
			echo mysqli_error($conex);
		}
		if($reclamo=='NO')
		{
			$UPDATE_HISTORIAL=mysqli_query($conex, "UPDATE bayer_historial_reclamacion SET  RECLAMO$dato='".$reclamo."',MOTIVO_NO_RECLAMACION$dato='".$causa_no_reclamacion."',FECHA_RECLAMACION$dato='' WHERE ID_PACIENTE_FK='".$codigo_usuario2."' AND MES$dato='".$mes_act."'");
			echo mysqli_error($conex);
		}
	}
	$select_temporal=mysqli_query($conex, "SELECT * FROM bayer_temporal_producto WHERE ID_PACIENTE_FK='".$codigo_usuario2."'");
	$nreg=mysqli_num_rows($select_temporal);
	if($nreg>0)
	{
		while($datos_temporales=(mysql_fetch_array($select_temporal)))
		{
			$tipo_envio=$datos_temporales['ID_REFERENCIA_FK'];
			$verificar_cantidad=mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE CANTIDAD>0 AND ID_REFERENCIA='$tipo_envio'");
			echo mysqli_error($conex);
			$cantidad=mysqli_num_rows($verificar_cantidad);
			if($cantidad>0)
			{
				$SELECT_ID_INV=mysqli_query($conex, "select ID_INVENTARIO from bayer_inventario WHERE LUGAR_MATERIAL='BODEGA' AND ID_REFERENCIA_FK='".$tipo_envio."' ORDER BY ID_INVENTARIO ASC LIMIT 1");
				echo mysqli_error($conex);
				while ($fila1 = mysqli_fetch_array($SELECT_ID_INV))
				{
					$ID_ULT_INV=$fila1['ID_INVENTARIO'];
				}
				/*$UPDATE_INVENTARIO=mysql_query("UPDATE bayer_inventario SET LUGAR_MATERIAL='".$codigo_usuario2."' WHERE ID_INVENTARIO='".$ID_ULT_INV."'",$conex);
				echo mysql_error($conex);*/
				$INSERT_MOVIMIENTO=mysqli_query($conex, "INSERT INTO bayer_movimientos(TIPO_MOVIMIENTO, NO_REMICION, CANTIDAD, RESPONSABLE, DESTINATARIO, DIRECCION_DESTINATARIO, CIUDAD_ENVIO, FECHA_MOVIMIENTO, OBSERVACIONES, ESTADO_MOVIMIENTO,ID_REFERENCIA_FK) VALUES('2', '', '1', '".$usuname."', '".$nombre.' '.$apellidos."', '".$direccion."', '".$ciudad."', CURRENT_TIMESTAMP, 'ENVIO PRODUCTO(S)', 'EN PROCESO','".$tipo_envio."')");
				echo mysqli_error($conex);
				$SELECT_CANTIDAD = mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE ID_REFERENCIA = '".$tipo_envio."'");
				echo mysqli_error($conex); 		
				while ($fila1 = mysqli_fetch_array($SELECT_CANTIDAD))
				{
					$CANTIDAD_I = $fila1['CANTIDAD'];	
				}
				$TOTAL=$CANTIDAD_I-1;
				$UPDATE_REFERENCIA=mysqli_query($conex, "UPDATE bayer_referencia SET CANTIDAD='".$TOTAL."' WHERE ID_REFERENCIA='".$tipo_envio."'");
				echo mysqli_error($conex);
				$SELECT_ID_MOVIMIENTO=mysqli_query($conex, "SELECT ID_MOVIMIENTOS FROM bayer_movimientos WHERE DESTINATARIO='".$nombre.' '.$apellidos."' AND TIPO_MOVIMIENTO='2' ORDER BY ID_MOVIMIENTOS DESC LIMIT 1");
				echo mysqli_error($conex);
				while ($fila_mov = mysqli_fetch_array($SELECT_ID_MOVIMIENTO))
				{
					$ID_ULT_MOVIMIENTO=$fila_mov['ID_MOVIMIENTOS'];
				}
				$INSERT_MOVIMIENTO_PACIENTE=mysqli_query($conex, "INSERT INTO bayer_paciente_movimientos(ID_PACIENTE_FK,ID_MOVIMIENTOS_FK,
		        ESTADO_PACIENTE_MOVIMIENTO)VALUES('".$codigo_usuario2."','".$ID_ULT_MOVIMIENTO."','EN PROCESO')");
				echo mysqli_error($conex);
				$INSERT_MOVIMIENTO_USUARIO=mysqli_query($conex, "INSERT INTO bayer_usuario_movimientos(ID_USUARIO_FK,ID_MOVIMIENTOS_FK)VALUES('".$id_usu."','".$ID_ULT_MOVIMIENTO."')");
				echo mysqli_error($conex);
				$verificar_cantidad=mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE ID_REFERENCIA='".$tipo_envio."' AND CANTIDAD<STOCK_MINIMO");
				echo mysqli_error($conex);
				$nreg_vrf=mysqli_num_rows($verificar_cantidad);
				?>
				<table style="margin:auto auto; font-size:80%;" >
				 <?php
				if($nreg_vrf>0)
				{
					while ($daro_ref = mysqli_fetch_array($verificar_cantidad))
					{
						$MATERIAL=$daro_ref['MATERIAL'];
						?>
						<tr align="left">
							<td align="left">
								<span class="error" style="font-size:100%; text-align:left">ADVERTENCIA SE ESTA AGOTANDO EL PRODUCTO <?php echo $MATERIAL?>
								</span>
							</td>
						</tr>
						<?php
					}
				}
			}
			else
			{
				$verificar_cantidad=mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE ID_REFERENCIA='".$tipo_envio."'");
				echo mysqli_error($conex);
				while ($cantidad = mysqli_fetch_array($verificar_cantidad))
				{
					$nombre_producto=$cantidad['MATERIAL'];
					?>
					<tr align="left">
						<td align="left">
							<span style="margin-top:3%;">
							<center>
							<img src="../presentacion/imagenes/advertencia2.png" width="52" height="50" style=" margin-top:100px;margin-top:5%;"/>
							</center>
							</span>
							<p class="error" style=" width:68.9%; margin:auto auto; font-size:80%;color:#F00; font-weight:bold">EL PRODUCTO &nbsp;&nbsp; <span style=""><?php echo $nombre_producto ?></span> &nbsp;&nbsp; ESTA AGOTADO POR FAVOR COMUNICARSE CON EL COORDINADOR.</p>
							<br />
							<br />
						<br/>
						</td>
					</tr>
					<?php
				}
			}
		}
			if($nreg_vrf>0)
			{
				?>	
					<tr>
						<td align="center">
						<span class="error" style="font-size:100%; ">POR FAVOR COMUNICARSE CON EL COORDINADOR.</span>
						<span>
							 <center>
							 <img src="../presentacion/imagenes/advertencia.png" width="52" height="50" style=" margin-top:100px;margin-top:5%;"/>
							 </center>
						 </span>
						 </td>
					</tr>
				<?php
			}
			?>
			</table>
			<?php
			$BORRAR_PRODUCTOS_TEMPORAL=mysqli_query($conex, "DELETE  FROM bayer_temporal_producto WHERE ID_PACIENTE_FK='".$codigo_usuario2."'");
			echo mysqli_error($conex);	
	}
	else
	{
		//$tipo_envio=$_POST['tipo_envio'];
		$tipo_envio='';
		$listado_envio=mysqli_query($conex, "SELECT MATERIAL,ID_REFERENCIA FROM bayer_referencia WHERE ID_REFERENCIA='".$tipo_envio."'");
        while($opcion=mysqli_fetch_array($listado_envio))
		{
			$nombre_producto=$opcion['MATERIAL'];
		}
		/*SI EL ENVIO ES KIT DE BIENVENIDA*/
		if($nombre_producto=='Kit de bienvenida')
		{
			//$tipo_envio=$_POST['tipo_envio'];
			$tipo_envio='';
			$verificar_cantidad=mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE CANTIDAD>0 AND ID_REFERENCIA='$tipo_envio'");
			echo mysqli_error($conex);
			$cantidad_ref=mysqli_num_rows($verificar_cantidad);
			if($cantidad_ref>0)
			{
					$verificar_cantidad=mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE CANTIDAD>0 AND ID_REFERENCIA='$tipo_envio'");
					echo mysqli_error($conex);
					$cantidad=mysqli_num_rows($verificar_cantidad);
					if($cantidad>0)
					{
						$SELECT_ID_INV=mysqli_query($conex, "select ID_INVENTARIO from bayer_inventario WHERE LUGAR_MATERIAL='BODEGA' AND ID_REFERENCIA_FK='".$tipo_envio."' ORDER BY ID_INVENTARIO ASC LIMIT 1");
						echo mysqli_error($conex);
						while ($fila1 = mysqli_fetch_array($SELECT_ID_INV))
						{
							$ID_ULT_INV=$fila1['ID_INVENTARIO'];
						}
						/*$UPDATE_INVENTARIO=mysql_query("UPDATE bayer_inventario SET LUGAR_MATERIAL='".$codigo_usuario2."' WHERE ID_INVENTARIO='".$ID_ULT_INV."'",$conex);
						echo mysql_error($conex);*/
						$INSERT_MOVIMIENTO=mysqli_query($conex, "INSERT INTO bayer_movimientos(TIPO_MOVIMIENTO, NO_REMICION, CANTIDAD, RESPONSABLE, DESTINATARIO, DIRECCION_DESTINATARIO, CIUDAD_ENVIO, FECHA_MOVIMIENTO, OBSERVACIONES, ESTADO_MOVIMIENTO,ID_REFERENCIA_FK) VALUES('2', '', '1', '".$usuname."', '".$nombre.' '.$apellidos."', '".$direccion."', '".$ciudad."', CURRENT_TIMESTAMP, 'ENVIO PRODUCTO(S)', 'EN PROCESO','".$tipo_envio."')");
						echo mysqli_error($conex);
						$SELECT_CANTIDAD = mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE ID_REFERENCIA = '".$tipo_envio."'");
						echo mysqli_error($conex); 
						while ($fila1 = mysqli_fetch_array($SELECT_CANTIDAD))
						{
							$CANTIDAD_I = $fila1['CANTIDAD'];	
						}
						$TOTAL=$CANTIDAD_I-1;
						$UPDATE_REFERENCIA=mysqli_query($conex, "UPDATE bayer_referencia SET CANTIDAD='".$TOTAL."' WHERE ID_REFERENCIA='".$tipo_envio."'");
						echo mysqli_error($conex);
						$SELECT_ID_MOVIMIENTO=mysqli_query($conex, "SELECT ID_MOVIMIENTOS FROM bayer_movimientos WHERE DESTINATARIO='".$nombre.' '.$apellidos."' AND TIPO_MOVIMIENTO='2' ORDER BY ID_MOVIMIENTOS DESC LIMIT 1");
						echo mysqli_error($conex);
						while ($fila_mov = mysqli_fetch_array($SELECT_ID_MOVIMIENTO))
						{
							$ID_ULT_MOVIMIENTO=$fila_mov['ID_MOVIMIENTOS'];
						}
						$INSERT_MOVIMIENTO_PACIENTE=mysqli_query($conex, "INSERT INTO bayer_paciente_movimientos(ID_PACIENTE_FK,ID_MOVIMIENTOS_FK,
				        ESTADO_PACIENTE_MOVIMIENTO)VALUES('".$codigo_usuario2."','".$ID_ULT_MOVIMIENTO."','EN PROCESO')");
						echo mysqli_error($conex);
						$INSERT_MOVIMIENTO_USUARIO=mysqli_query($conex, "INSERT INTO bayer_usuario_movimientos(ID_USUARIO_FK,ID_MOVIMIENTOS_FK)VALUES('".$id_usu."','".$ID_ULT_MOVIMIENTO."')");
						echo mysqli_error($conex);
						$BORRAR_PRODUCTOS_TEMPORAL=mysqli_query($conex, "DELETE  FROM bayer_temporal_producto WHERE ID_PACIENTE_FK='".$codigo_usuario2."'");
						echo mysqli_error($conex);
						$verificar_cantidad=mysqli_query($conex, "SELECT ID_REFERENCIA FROM bayer_referencia WHERE ID_REFERENCIA='".$tipo_envio."' AND CANTIDAD<STOCK_MINIMO");
						echo mysqli_error($conex);
						$nreg_vrf=mysqli_num_rows($verificar_cantidad);
						if($nreg_vrf>0)
						{
							?> 
								<span style="margin-top:3%;">
									 <center>
									 <img src="../presentacion/imagenes/advertencia.png" width="52" height="50" style=" margin-top:100px;margin-top:5%;"/>
									 </center>
				</span>
									 <p class="error" style=" width:68.9%; margin:auto auto; font-size:80%;">ADVERTENIA SE ESTA AGOTANDO EL PRODUCTO &nbsp;&nbsp; <span style="color:#F00; font-weight:bold"><?php echo $nombre_producto ?></span> &nbsp;&nbsp; POR FAVOR COMUNICARSE CON EL COORDINADOR.</p>
									 <br />
									 <br />
								<br/>
							<?php
						}
					}
					else
					{
						?> 
								<span style="margin-top:3%;">
									 <center>
									 <img src="../presentacion/imagenes/advertencia2.png" width="52" height="50" style=" margin-top:100px;margin-top:5%;"/>
									 </center>
				</span>
									 <p class="error" style=" width:68.9%; margin:auto auto; font-size:80%;color:#F00; font-weight:bold">EL PRODUCTO &nbsp;&nbsp; <span style=""><?php echo $nombre_producto ?></span> &nbsp;&nbsp; ESTA AGOTADO POR FAVOR COMUNICARSE CON EL COORDINADOR.</p>
									 <br />
									 <br />
								<br/>
							<?php
					}
				}
				else
				{
					$verificar_cantidad=mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE ID_REFERENCIA='".$tipo_envio."'");
					echo mysqli_error($conex);
					while ($cantidad = mysqli_fetch_array($verificar_cantidad))
					{
						$nombre_producto=$cantidad['MATERIAL'];
						?>
						<tr align="left">
							<td align="left">
								<span style="margin-top:3%;">
								<center>
								<img src="../presentacion/imagenes/advertencia2.png" width="52" height="50" style=" margin-top:100px;margin-top:5%;"/>
								</center>
								</span>
								<p class="error" style=" width:68.9%; margin:auto auto; font-size:80%;color:#F00; font-weight:bold">EL PRODUCTO &nbsp;&nbsp; <span style=""><?php echo $nombre_producto ?></span> &nbsp;&nbsp; ESTA AGOTADO POR FAVOR COMUNICARSE CON EL COORDINADOR.</p>
								<br />
								<br />
							<br/>
							</td>
						</tr>
						<?php
					}
				}
		}
	}
	$sql=mysqli_query($conex, "UPDATE bayer_gestiones 
	SET ESTADO_GESTION='GESTIONADO'
	WHERE ID_GESTION='".$codigo_gestion."'");
	echo mysqli_error($conex);
	//	FECHA_COMUNICACION=CURRENT_TIMESTAMP
if($logro_comunicacion=='SI')
{
	$sql=mysqli_query($conex, "UPDATE bayer_pacientes SET ESTADO_PACIENTE='".$estado_paciente."', STATUS_PACIENTE='".$status_paciente."', FECHA_ACTIVACION_PACIENTE='".$fecha_activacion."', FECHA_RETIRO_PACIENTE='".$fecha_retiro."', MOTIVO_RETIRO_PACIENTE='".$motivo_retiro."', OBSERVACION_MOTIVO_RETIRO_PACIENTE='".$observacion_retiro."', TELEFONO_PACIENTE='".$telefono1."', TELEFONO2_PACIENTE='".$telefono2."', TELEFONO3_PACIENTE='".$telefono3."', CORREO_PACIENTE='".$correo."', DIRECCION_PACIENTE='".$direccion."', BARRIO_PACIENTE='".$barrio."', DEPARTAMENTO_PACIENTE='".$departamento."',CIUDAD_PACIENTE='".$ciudad."',FECHA_NACIMINETO_PACIENTE='".$fecha_nacimiento."',EDAD_PACIENTE='".$edad."' WHERE ID_PACIENTE='".$codigo_usuario2."'");
	echo mysqli_error($conex);
	$sql=mysqli_query($conex, "UPDATE bayer_tratamiento SET TRATAMIENTO_PREVIO='".$tratamiento_previo."', ASEGURADOR_TRATAMIENTO='".$asegurador."', OPERADOR_LOGISTICO_TRATAMIENTO='".$operador_logistico."',FECHA_ULTIMA_RECLAMACION_TRATAMIENTO='".$fecha_ultima_reclamacion."',PUNTO_ENTREGA='".$punto_entrega."',MEDICO_TRATAMIENTO='".$medico."',IPS_ATIENDE_TRATAMIENTO='".$ips_atiende."',DOSIS_TRATAMIENTO='".$dosis."',FECHA_INICIO_TERAPIA_TRATAMIENTO='".$fecha_ini_terapia."' WHERE ID_PACIENTE_FK='".$codigo_usuario2."'");
	echo mysqli_error($conex);
}
	$sql=mysqli_query($conex, "INSERT INTO bayer_gestiones (MOTIVO_COMUNICACION_GESTION,MEDIO_CONTACTO_GESTION,TIPO_LLAMADA_GESTION,LOGRO_COMUNICACION_GESTION,MOTIVO_NO_COMUNICACION_GESTION,NUMERO_INTENTOS_GESTION,ESTADO_CTC_GESTION,ESTADO_FARMACIA_GESTION,RECLAMO_GESTION,CONSECUTIVO_BETAFERON,CAUSA_NO_RECLAMACION_GESTION,DIFICULTAD_ACCESO_GESTION,TIPO_DIFICULTAD_GESTION,ENVIOS_GESTION,MEDICAMENTOS_GESTION,TIPO_ENVIO_GESTION,EVENTO_ADVERSO_GESTION,TIPO_EVENTO_ADVERSO,GENERA_SOLICITUD_GESTION,FECHA_PROXIMA_LLAMADA,MOTIVO_PROXIMA_LLAMADA,OBSERVACION_PROXIMA_LLAMADA,FECHA_RECLAMACION_GESTION,NUMERO_CAJAS,CONSECUTIVO_GESTION,AUTOR_GESTION,NOTA,DESCRIPCION_COMUNICACION_GESTION,FECHA_PROGRAMADA_GESTION,USUARIO_ASIGANDO,ID_PACIENTE_FK2,FECHA_COMUNICACION,NUMERO_NEBULIZACIONES,NUMERO_TABLETAS_DIARIAS)VALUES('".$motivo_comunicacion."','".$medio_contacto."','".$tipo_llamada."','".$logro_comunicacion."','".$motivo_no_comunicacion."','".$via_recepcion."','".$estado_ctc."','".$estado_farmacia."','".$reclamo."','".$consecutivo_betaferon."','".$CAUSA_NO_RECLAMACION_GESTION."','".$dificultad_acceso."','".$tipo_dificultad."','".$envios."','".$MEDICAMENTO."','".$tipo_envio."','".$evento_adverso."','".$tipo_evento_adverso."','".$genera_solicitud."','".$fecha_proxima_llamada."','".$motivo_proxima_llamada."','".$observacion_proxima_llamada."','".$fecha_reclamacion."','".$numero_cajas."','".$consecutivo."','".$autor."','".$nota."','".$descripcion_comunicacion."','".$fecha_proxima_llamada."','SIN ASIGNAR','".$codigo_usuario2."',CURRENT_TIMESTAMP,'".$numero_nebulizaciones."','".$numero_tabletas_diarias."')");
    echo mysqli_error($conex);
$select_gestion=mysqli_query($conex, "SELECT * FROM bayer_gestiones WHERE ID_PACIENTE_FK2='".$codigo_usuario2."' ORDER BY ID_GESTION DESC LIMIT 1");
while($datos_gestion=mysqli_fetch_array($select_gestion))
{
	$ID_ULTIMA_GESTION=$datos_gestion['ID_GESTION'];
}
$update_codigo_gestion=mysqli_query($conex, "UPDATE bayer_pacientes SET ID_ULTIMA_GESTION='".$ID_ULTIMA_GESTION."' 
WHERE ID_PACIENTE='".$codigo_usuario2."'");
echo mysqli_error($conex);
if ($_FILES['archivo']["error"] > 0)
{
}
else
{
	$SELECT_GES=mysqli_query($conex, "SELECT ID_GESTION FROM bayer_gestiones ORDER BY ID_GESTION DESC LIMIT 1");
	while ($fila2 = mysqli_fetch_array($SELECT_GES))
	{
		$ID_GES=$fila2['ID_GESTION'];
	}
	$CARPETA = "../ADJUNTOS_BAYER/$ID_GES";
	if(!is_dir($CARPETA))
	{ 
		mkdir("../ADJUNTOS_BAYER/$ID_GES",0777); 
	}
	move_uploaded_file($_FILES['archivo']['tmp_name'],"../ADJUNTOS_BAYER/$ID_GES/" . $_FILES['archivo']['name']);
}
	if($sql)
	{
		if($evento_adverso=='SI')
		{
			if($tipo_evento_adverso=='Farmacovigilancia'||$tipo_evento_adverso=='Tecnovigilancia Betaconnet/ Omrron')
			{
					?> 
                    <span style="margin-top:5%;">
                         <center>
                         <img src="../presentacion/imagenes/chulo.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
                         </center>
                         </span>
                         <p class="aviso3" style=" width:68.9%; margin:auto auto;">EL SEGUMIENTO HA SIDO INGRESADO SATISFACTORIAMENTE.</p>
                         <br />
                         <br />
                          <center>
                          <a href="../presentacion/form_paciente_seguimiento.php" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" /></a>
                          </center>
                    <br/>
					<?php
				}
			else
			{
				if($tipo_evento_adverso!='Farmacovigilancia'||$tipo_evento_adverso!='Tecnovigilancia Betaconnet/ Omrron')
				{
					?> 
                    <span style="margin-top:5%;">
                         <center>
                         <img src="../presentacion/imagenes/chulo.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
                         </center>
                         </span>
                         <p class="aviso3" style=" width:68.9%; margin:auto auto;">EL SEGUMIENTO HA SIDO INGRESADO SATISFACTORIAMENTE.</p>
                         <br />
                         <br />
                          <center>
                          <a href="../presentacion/form_paciente_seguimiento.php" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" /></a>
                          </center>
                    <br/>
					<?php
				}	
			}
		}
		else
		if($evento_adverso!='SI')
		{
		?> 
			<span style="margin-top:5%;">
				 <center>
				 <img src="../presentacion/imagenes/chulo.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
				 </center>
				 </span>
				 <p class="aviso3" style=" width:68.9%; margin:auto auto;">EL SEGUMIENTO HA SIDO INGRESADO SATISFACTORIAMENTE.</p>
				 <br />
				 <br />
				  <center>
				  <a href="../presentacion/form_paciente_seguimiento.php" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" /></a>
				  </center>
			<br/>
		<?php
		}
	}
	else
	{
		?> 
			<span style="margin-top:5%;">
				 <center>
				 <img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
				 </center>
				 </span>
				 <p class="error" style=" width:68.9%; margin:auto auto;">EL SEGUMIENTO NO HA SIDO INGRESADO SATISFACTORIAMENTE.</p>
				 <br />
				 <br />
				  <center>
				  <a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA.png" style="width:152px; height:37px" /></a>
				  </center>
			<br/>
		<?php
	}
}
}